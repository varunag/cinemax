<?php
	session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Cinemax</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="images/favicon.ico" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/kunjStyle.css"/>
<script type="text/javascript" src="js/typed.js"></script>

<script>
    function typed(){
        Typed.new("#typed", {
            stringsElement: document.getElementById('typed-strings'),
            typeSpeed: 30,
            backDelay: 500,
            loop: null,
            contentType: 'html', // or text
            // defaults to null for infinite loop
            loopCount: null,
            callback: function(){ foo(); },
            resetCallback: function() { newTyped(); }
        });
        var resetElement = document.querySelector('.reset');
        if(resetElement) {
            resetElement.addEventListener('click', function() {
                document.getElementById('typed')._typed.reset();
            });
        }
	};
    function newTyped(){ /* A new typed object */ }
    function foo(){ console.log("Callback"); }
    
    </script>
    <style>
	
        /* code for animated blinking cursor */
        .typed-cursor{
            opacity: 1;
            font-weight: 100;
            -webkit-animation: blink 0.7s infinite;
            -moz-animation: blink 0.7s infinite;
            -ms-animation: blink 0.7s infinite;
            -o-animation: blink 0.7s infinite;
            animation: blink 0.7s infinite;
        }
        @-keyframes blink{
            0% { opacity:1; }
            50% { opacity:0; }
            100% { opacity:1; }
        }
        @-webkit-keyframes blink{
            0% { opacity:1; }
            50% { opacity:0; }
            100% { opacity:1; }
        }
        @-moz-keyframes blink{
            0% { opacity:1; }
            50% { opacity:0; }
            100% { opacity:1; }
        }
        @-ms-keyframes blink{
            0% { opacity:1; }
            50% { opacity:0; }
            100% { opacity:1; }
        }
        @-o-keyframes blink{
            0% { opacity:1; }
            50% { opacity:0; }
            100% { opacity:1; }
        }
		#typed-strings{
			font-family:"Courier New", Courier, monospace !important;
			font-size:30px !important;
		}
		.wrap{
			max-width: 600px;
			margin:150px auto;
		}
		
		.type-wrap{
			margin:10px auto;
			padding:20px;
			background:#f0f0f0;
			border-radius:5px;
			border:#CCC 1px solid;
			font:"Courier New", Courier, monospace;
			font-family:"Courier New", Courier, monospace;
			font-size:24px;
		}
    </style>



</head>
<body id="page1" onload="typed()">
<!-- START PAGE SOURCE -->
<div class="tail-top">
  <div class="tail-bottom">
    <div id="main">
      <div id="header">
        <div class="row-1">
          <div class="fleft"><a href="#">SIT <span>CINEMAX</span></a></div>
          <ul>
            <li><a href="#"><img src="images/icon1-act.gif" alt="" /></a></li>
            <li><a href="#"><img src="images/icon2.gif" alt="" /></a></li>
            <li><a href="#"><img src="images/icon3.gif" alt="" /></a></li>
          </ul>
        </div>
        <div class="row-2">
          <ul>
            <li><a href="index.php" class="active">Home</a></li>
            <li><a href="about-us.html">About</a></li>
            <li><a href="articles.html">Movie Library</a></li>
            <li><a href="contact-us.html">Contacts</a></li>
            <li class="last"><a href="sitemap.html">Rules & Regulations</a></li>
          </ul>
        </div>
      </div>
      <div id="content">
        <div id="slogan">
          <div class="image png"></div>
          <div class="inside">
          	<div class="type-wrap">
                <div id="typed-strings">
                   <span style="font-size:16px;">Welcome To Cinemax</span>
                   <p style="font-size:16px;">Take <span>A Break!!</span></p>
                   <p>We Show You Movies.</p>
                </div>
                    <span id="typed" style="white-space:pre;"></span>
        	</div>
            
       
            <p>We the Cine Club of SIT have taken initiative to give an opportunity to the students of our college to watch a diverse range of films including mainstream and blockbusters for entertainment and mental refreshment purpose.</p>
           
          </div>
        </div>
		
        <div class="box">
          <div class="border-right">
            <div class="border-left">
               <!--Starting of Login section -->
			   <center style="padding-top:30px">
				<h1><b><u>Welcome to CineMax</u></b></h1>
                <br></br>
                <h1><b><u>Login</u></b></h1>
			   </center>
				<div id="login">
				<center>
					<p style="margin-left:20px ; margin-right: 20px; font-size: 30px; color: red">
						<?php
                        if(isset($_SESSION["error"])){
                            echo $_SESSION["error"];
                            unset($_SESSION["error"]);
                        }
                        if(isset($_SESSION["error1"])){
                            echo $_SESSION["error1"];
                            unset($_SESSION["error1"]);
                        }
                        ?>
					</p><br>
					<form method="post" action="login.php">
						SIC NO. :&nbsp;&nbsp;
						<input type="text" name="sic" pattern="[A-Za-z0-9]{8}" title="Enter Sic No. Correctly">
						<br><br>
						Password :
						<input type="password" name="pass">
						<br><br><br>
						<span style="padding-left:10%;"></span>
						<input type="submit" class="button button1" name="login" value="Login">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="Register.html"><input type="button" class="button button2" value="Register"></a>
						
						<br><br><br><br><br><br>
					</form>
				</center>
				</div>
            </div>
           </div>
        </div>
      </div>
	  <center style="padding-top:30px"> <h1><u>Movies This Week</u></h1></center>
        <div class="content" style="padding-top:30px">
          <ul class="movies">
            <li>
              <h4 style="padding-left:100px">Avatar</h4>
              <center><img src="images/1page-img2.jpg" alt="" /></center>
              <p>A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and
			  protecting the world he feels is his home.<br /><br /></p>
             <div class="wrapper" style="padding-left:30px"><a href="http://www.imdb.com/title/tt0499549/" class="link2"><span>
			  <span>Read More</span></span></a></div>
            </li>
			
            <li>
              <h4 style="padding-left:100px">3 Idiots</h4>
              <center><img src="images/1page-img3.jpg" alt="" /></center>
              <p>Two friends are searching for their long lost companion. They revisit their college days and recall the memories of their friend who inspired them to think differently, even as the rest of the world called them "idiots". </p>
              <div class="wrapper" style="padding-left:30px"><a href="http://www.imdb.com/title/tt1187043/" class="link2"><span><span>Read More</span></span></a></div>
            </li>
			
            <li class="last">
              <h4 style="padding-left:30px">The Twilight Saga: Eclipse</h4>
              <center><img src="images/1page-img4.jpg" alt="" /></center>
              <p>As a string of mysterious killings grips Seattle, Bella, whose high school graduation is fast approaching, is forced to choose between her love for vampire Edward and her friendship with werewolf Jacob.</p>
              <div class="wrapper" style="padding-left:30px"><a href="http://www.imdb.com/title/tt1325004/?ref_=nv_sr_2" class="link2"><span><span>Read More</span></span></a></div>
            </li>
            <li class="clear">&nbsp;</li>
          </ul>
        </div>
      </div>
      <div id="footer">
        <div class="left">
          <div class="right">
            <div class="footerlink">
              <p class="lf">Copyright &copy; 2017 <a href="#">SIT CINEMAX</a> - All Rights Reserved</p>
              <p class="rf"><B>Designed by :- VARUN AGRAWAL [IT]  ARUNAYA KUNJ [IT] </B></</p>
              <div style="clear:both;"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END PAGE SOURCE -->
</body>
</html>