<?php
	session_start();
	if(!isset($_SESSION["Name"]))
	{
        $_SESSION["error1"] = 'You Are Not Authorized To Access This Web Page!!!!<br>Please LogIn To Continue';
        echo $_SESSION["error1"];
		header('Location:index.php');
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Cinemax</title>
<link rel="icon" href="images/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/myJs.js"></script>
<link rel="stylesheet" type="text/css" href="css/vStyle.css"/>
<script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="js/cufon-yui.js" type="text/javascript"></script>
<script src="js/cufon-replace.js" type="text/javascript"></script>
<script src="js/Gill_Sans_400.font.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>

	<?php
		include 'serverSide/header.php';
	?>

<style>
	button{
		background-color: buttonface; 
		border: none;
		color: white;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 16px;
		margin: 4px 2px;
		-webkit-transition-duration: 0.4s; /* Safari */
		transition-duration: 0.4s;
		cursor: pointer;
	}
	button:hover {
		background-color: #999;
		color: black;
	}
	input[type=submit]{
		background-color: white /* Green */
		border: none;
		color: #008CBA;
		padding: 16px 32px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 16px;
		margin: 4px 2px;
		-webkit-transition-duration: 0.4s; /* Safari */
		transition-duration: 0.4s;
		cursor: pointer; 
		background-color: white; 
		color: black; 
		border: 2px solid #008CBA;;
	}
	input[type=submit]:hover {
		background-color: #008CBA;
    	color: white;
	}
</style>


</head>
<body id="page1">
<!-- START PAGE SOURCE -->
<div class="tail-top">
  <div class="tail-bottom">
    <div id="main">
      <div id="header">
        <div class="row-1">
          <div class="fleft"><a href="#">SIT <span>CINEMAX</span></a></div>
          <div class="wel">
         		<span>Welcome <?php echo $_SESSION["Name"] ?> </span>
                <span style="padding-left:20px;"><a href="php/logout.php">logout</a>
          </div>
        </div>
        <div class="row-2">
          <ul>
            <li><a href="booktkt.php" class="active">Ticket Booking</a></li>
            <li><a href="canceltkt.php">Cancel Tickets</a></li>
          </ul>
        </div>
      </div>
      <div id="content">
        <div id="slogan">
          <div class="inside seldate">
            <span id="datesapn" ><span style="font-family:'Courier New', Courier, monospace; font-size:16px;">Select Date:</span><input type="date" id="selDate" onchange="testdate(this,true)" /></span><br /><br />
            <span id="mLabel" hidden="true"  style="font-family:'Courier New', Courier, monospace; font-size:16px;">Movie Name:</span><span id="mName"></span><br />
            <p id="showList" style="font-family:'Courier New', Courier, monospace; font-size:16px;" ></p>
            <br />
          </div>
        </div>
        <div class="box">
        	<div class="border-right">
            	<div class="border-left">
               		<p id="forspace"><br>
		                <span style="margin-left: 30%; font-size: 30px; color: red">
			                <?php
			                    if(isset($_SESSION["error"])){
			                        echo $_SESSION["error"];
			                        unset($_SESSION["error"]);
			                    }
			                ?>
		                </span>
	                </p>
              			<div id="seatLayout" hidden="true">
                   			<center><table border="0" style="color:white" align="center" class="seatFormat" cellpadding="1" cellspacing="5">
                      <tr id="number">
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>1</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                        <td>5</td>
                        <td>6</td>
                        <td>7</td>
                        <td rowspan="17">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>8</td>
                        <td>9</td>
                        <td>10</td>
                        <td>11</td>
                        <td>12</td>
                        <td>13</td>
                        <td>14</td>
                        <td>15</td>
                        <td>16</td>
                        <td>17</td>
                        <td>18</td>
                        <td>19</td>
                        <td>20</td>
                        <td>21</td>
                        <td rowspan="17">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>22</td>
                        <td>23</td>
                        <td>24</td>
                        <td>25</td>
                        <td>26</td>
                        <td>27</td>
                        <td>28</td>
                      </tr>
                      <tr> 
                        <td>M</td> 
                        <td><button onclick="seatSelect(this.id)" id="M1" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td> 
                        <td><button onclick="seatSelect(this.id)" id="M2" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td> 
                        <td><button onclick="seatSelect(this.id)" id="M3" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td> 
                        <td><button onclick="seatSelect(this.id)" id="M4" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td> 
                        <td><button onclick="seatSelect(this.id)" id="M5" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td> 
                        <td><button onclick="seatSelect(this.id)" id="M6" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td> 
                        <td><button onclick="seatSelect(this.id)" id="M7" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td> 
                        <td><button onclick="seatSelect(this.id)" id="M8" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="M9" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="M10" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td colspan="7" align="center">PROJECTOR ROOM</td>  
                        <td><button onclick="seatSelect(this.id)" id="M18" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="M19" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="M20" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="M21" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="M22" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="M23" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="M24" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="M25" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="M26" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="M27" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td> 
                        <td><button onclick="seatSelect(this.id)" id="M28" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td> 
                      </tr> 
                      <tr> 
                        <td>L</td> 
                        <td><button onclick="seatSelect(this.id)" id="L1" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="L2" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="L3" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="L4" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="L5" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="L6" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="L7" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="L8" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="L9" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="L10" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="L11" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="L12" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="L13" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="L14" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="L15" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="L16" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="L17" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="L18" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="L19" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="L20" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="L21" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="L22" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="L23" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="L24" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="L25" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="L26" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="L27" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="L28" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                      </tr> 
                      <tr> 
                        <td>K</td> 
                        <td><button onclick="seatSelect(this.id)" id="K1" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="K2" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="K3" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="K4" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="K5" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="K6" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="K7" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="K8" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="K9" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="K10" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="K11" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="K12" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="K13" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="K14" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="K15" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="K16" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="K17" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="K18" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="K19" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="K20" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="K21" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="K22" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="K23" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="K24" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="K25" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="K26" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="K27" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                        <td><button onclick="seatSelect(this.id)" id="K28" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td> 
                      </tr> 
                      <tr> 
                        <td>J</td> 
                        <td><button onclick="seatSelect(this.id)" id="J1" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="J2" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="J3" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="J4" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="J5" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="J6" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="J7" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="J8" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="J9" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="J10" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="J11" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="J12" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="J13" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="J14" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="J15" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="J16" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="J17" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="J18" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="J19" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="J20" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="J21" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="J22" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="J23" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="J24" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="J25" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="J26" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="J27" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="J28" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                      </tr> 
                      <tr> 
                        <td>I</td> 
                        <td><button onclick="seatSelect(this.id)" id="I1" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="I2" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="I3" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="I4" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="I5" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="I6" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="I7" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="I8" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="I9" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="I10" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="I11" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="I12" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="I13" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="I14" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="I15" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="I16" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="I17" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="I18" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="I19" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="I20" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="I21" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="I22" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="I23" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="I24" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="I25" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="I26" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="I27" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="I28" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                      </tr> 
                      <tr> 
                        <td>H</td> 
                        <td><button onclick="seatSelect(this.id)" id="H1" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="H2" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="H3" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="H4" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="H5" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="H6" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="H7" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="H8" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="H9" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="H10" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="H11" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="H12" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="H13" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="H14" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="H15" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="H16" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="H17" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="H18" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="H19" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="H20" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="H21" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="H22" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="H23" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="H24" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="H25" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="H26" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="H27" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="H28" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                      </tr> 
                      <tr> 
                        <td>G</td> 
                        <td><button onclick="seatSelect(this.id)" id="G1" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="G2" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="G3" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="G4" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="G5" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="G6" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="G7" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="G8" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="G9" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="G10" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="G11" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="G12" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="G13" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="G14" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="G15" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="G16" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="G17" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="G18" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="G19" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="G20" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="G21" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="G22" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="G23" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="G24" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="G25" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="G26" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="G27" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="G28" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                      </tr> 
                      <tr> 
                        <td>F</td> 
                        <td colspan="7">&nbsp;</td> 
                        <td colspan="14" style="color:white;" align="center">Booked For SIT Cinemax Club</td> 
                        <td colspan="7">&nbsp;</td> 
                      </tr> 
                      <tr> 
                        <td colspan="31">&nbsp;</td> 
                         
                      </tr> 
                      <tr> 
                        <td>E</td> 
                        <td><button onclick="seatSelect(this.id)" id="E1" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="E2" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="E3" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="E4" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="E5" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="E6" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="E7" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="E8" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="E9" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="E10" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="E11" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="E12" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="E13" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="E14" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="E15" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="E16" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="E17" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="E18" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="E19" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="E20" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="E21" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="E22" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="E23" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="E24" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="E25" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="E26" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="E27" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="E28" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                      </tr> 
                      <tr> 
                        <td>D</td> 
                        <td><button onclick="seatSelect(this.id)" id="D1" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="D2" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="D3" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="D4" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="D5" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="D6" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="D7" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="D8" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="D9" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="D10" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="D11" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="D12" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="D13" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="D14" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="D15" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="D16" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="D17" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="D18" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="D19" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="D20" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="D21" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="D22" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="D23" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="D24" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="D25" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="D26" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="D27" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="D28" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                      </tr> 
                      <tr> 
                        <td>C</td> 
                        <td><button onclick="seatSelect(this.id)" id="C1" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="C2" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="C3" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="C4" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="C5" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="C6" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="C7" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="C8" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="C9" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="C10" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="C11" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="C12" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="C13" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="C14" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="C15" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="C16" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="C17" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="C18" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="C19" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="C20" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="C21" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="C22" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="C23" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="C24" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="C25" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="C26" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="C27" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>   
                        <td><button onclick="seatSelect(this.id)" id="C28" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
                      </tr> 
                      <tr> 
                        <td>B</td> 
                        <td><button onclick="seatSelect(this.id)" id="B1" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="B2" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="B3" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="B4" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="B5" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="B6" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="B7" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="B8" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="B9" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="B10" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="B11" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="B12" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="B13" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="B14" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="B15" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="B16" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="B17" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="B18" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="B19" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="B20" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="B21" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="B22" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="B23" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="B24" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="B25" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="B26" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="B27" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="B28" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>  
      
                      </tr> 
                      <tr> 
                        <td>A</td> 
                        <td><button onclick="seatSelect(this.id)" id="A1" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="A2" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="A3" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="A4" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="A5" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="A6" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="A7" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="A8" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="A9" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="A10" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="A11" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="A12" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="A13" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="A14" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="A15" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="A16" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="A17" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="A18" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="A19" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="A20" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="A21" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="A22" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="A23" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="A24" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="A25" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="A26" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>     
                        <td><button onclick="seatSelect(this.id)" id="A27" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td>    
                        <td><button onclick="seatSelect(this.id)" id="A28" class="seats">&nbsp;&nbsp;&nbsp;&nbsp;</button></td> 
                      </tr>
                      <tr>
                        <td colspan="31">&nbsp;</td>
                      </tr>
                      <tr>
                        <td colspan="31" align="center" style="color:white;">Screen This Way</td>
                      </tr>
                      <tr>
                        <td colspan="31">&nbsp;</td>
                      </tr>
                    </table></center>
                    <form action="confirm.php" method="get" >
                        <center><input type="submit" name="submit" value="Proceed To Book" /></center>
                    		</form>
                            <br />
          				</div>
      			</div>
      		</div>
      <div id="footer">
        <div class="left">
          <div class="right">
            <div class="footerlink">
              <p class="lf">Copyright &copy; 2017 <a href="#">SIT CINEMAX</a> - All Rights Reserved</p>
              <p class="rf"><B>Designed by :- VARUN AGRAWAL [IT]   ARUNAYA KUNJ [IT] </B></</p>
              <div style="clear:both;"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END PAGE SOURCE -->
</body>
</html>