-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 10, 2017 at 06:28 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cinemax`
--

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `SIC` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`SIC`) VALUES
('IT134670'),
('IT134679'),
('IT123456'),
('IT123456'),
('IT114744');

-- --------------------------------------------------------

--
-- Table structure for table `movie_list`
--

CREATE TABLE IF NOT EXISTS `movie_list` (
  `name` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `no_of_shows` varchar(1) NOT NULL,
  `pic` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `movie_list`
--

INSERT INTO `movie_list` (`name`, `date`, `no_of_shows`, `pic`) VALUES
('Terminator', '2017-01-25', '2', 'uploads/emilia_clarke_in_hbo_game_of_thrones-1280x'),
('Captain America : Civil War', '2017-01-23', '5', 'uploads/emilia_clarke_6-1280x800.jpg'),
('Avengers', '2017-02-28', '2', 'uploads/2-1024x640.jpeg'),
('Baywatch', '2017-06-01', '3', 'uploads/IMG_1741.JPG'),
('Baywatch', '2017-06-01', '3', 'uploads/batman-arkham-knight-e3-screen-3.jpg'),
('POTC', '2017-06-02', '3', 'uploads/');

-- --------------------------------------------------------

--
-- Table structure for table `user_db`
--

CREATE TABLE IF NOT EXISTS `user_db` (
  `NAME` varchar(50) NOT NULL,
  `SIC_NO` varchar(8) NOT NULL,
  `PASSWORD` varchar(20) NOT NULL,
  `EMAIL` varchar(50) NOT NULL,
  `CONTACT` int(10) NOT NULL,
  PRIMARY KEY (`SIC_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_db`
--

INSERT INTO `user_db` (`NAME`, `SIC_NO`, `PASSWORD`, `EMAIL`, `CONTACT`) VALUES
('Tarini Mishra', 'FCS05247', 'Abc123', 'tarini@silicon.ac.in', 0),
('Varun', 'IT134670', 'Honey2410@', 'varun241095@rediffmail.com', 2147483647),
('Vishal', 'IT134671', 'Asdfghi34', 'vishal.chotu@gmail.com', 2147483647),
('Bhuban', 'IT134948', 'pageDown14@', 'bhubanbam@bbkivines.com', 1234567890),
('Riaaaa', 'IT135037', 'Asdf123@', 'riaaaa@aaaa.com', 1234567890);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
