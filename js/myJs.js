//Sends a JSONP request to see whether there is any movie present on the day selected by the user
	function testdate()
	{
		var d = new Date(document.getElementById("selDate").value);
		mo = d.getMonth()+1;
		if(mo>=1&&mo<=9)
		{
			fDate = d.getFullYear() + "-" + 0+mo + "-" + d.getDate();
		}
		else
		{
			fDate = d.getFullYear() + "-" + mo + "-" + d.getDate();
		}

        var request = $.ajax({
            url: "php/getList.php",
            type: "POST",
            data: {
            	date:fDate,
			},
            dataType: "html",
            beforeSend : function()    {
                if(request != null) {
                    currentRequest.abort();
                    console.log("aborted");
                }
            },
        });

        request.done(function(data) {
            myFunc(data);
        });


        request.fail(function(jqXHR, textStatus) {
            alert( "Request failed: " + textStatus );
        });
	}
	
	
	//Shows Whether There Is Any Movie On that day and if there is a movie how many shows are present
	function myFunc(myObj) {
		if(myObj != 'null')
		{
			myObj = JSON.parse(myObj);
			document.getElementById("mLabel").hidden = false;
			document.getElementById("mName").hidden = false;
			document.getElementById("showList").hidden = false;
			document.getElementById("mName").innerHTML = myObj["name"];
			var txt = "";
			for(i = 1; i <= myObj.no_of_shows; i++ )
			{
				txt = txt + '<input type="radio" name="show" id="show' + i + '" onclick="showInfo(this.id)" />Show ' + i + '<br />';
			}

			document.getElementById("showList").innerHTML = txt;
            document.getElementById("forspace").style.height = '200px';
		}
		else{
			alert("Not On This Day");
			document.getElementById("seatLayout").hidden = true;
			document.getElementById("mName").hidden = true;
			document.getElementById("mLabel").hidden = true;
			document.getElementById("showList").hidden = true;
            document.getElementById("forspace").style.height = '290px';
		}
	}
	
	
	//Shows Which Seats Are Already Booked Of The Show Selected By The User
	function showInfo(id)
	{
		var s1 = document.getElementById(id);
		
		if(s1.checked)
		{
            var request = $.ajax({
                url: "php/avaiSeats.php",
                type: "POST",
				data: {show_no:id},
                dataType: "html",
                beforeSend: function () {
                    if (request != null) {
                        currentRequest.abort();
                    }
                },
            });

            request.done(function (myObj) {
				aSeats(myObj);
            });


            request.fail(function (jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });
		}
	}
	
	
	//marks all the seats that are already booked of the show selected by the user
	function aSeats(myObj1)
	{

		aSeats.count = aSeats.count || 0;

		document.getElementById("seatLayout").hidden = false;
		document.getElementById("forspace").hidden = true;
		seatDetails = myObj1;
		myObj1 = JSON.parse(myObj1);

		//For Any Previous Show No Selected it should clear the Selected seats.
		if(aSeats.count > 0)
		{
			obj2 = aSeats.prev;
			for(x in obj2)
			document.getElementById(obj2[x].seat_no).style.backgroundColor = "buttonface";
		}
		for(x in myObj1)
		{
		    now = myObj1[x];
			aSeats.count++;
			document.getElementById(myObj1[x].seat_no).style.backgroundColor = "red";
			aSeats.prev = myObj1;
		}
		checkVIP();
	}
	
	function checkVIP() {
        var request = $.ajax({
            url: "php/checkVIP.php",
            type: "GET",
            dataType: "html",
            beforeSend: function () {
                if (request != null) {
                    currentRequest.abort();
                }
            },
        });

        request.done(function (myObj) {
        	myObj = JSON.parse(myObj);
            if (myObj.member == 0) {
                for (i = 'M'.charCodeAt(0); i >= 'G'.charCodeAt(0); i--) {
                    for (j = 8; j <= 21; j++) {
                        if (i == 77 && j == 11) {
                            j = 18;
                        }
                        document.getElementById(String.fromCharCode(i) + j).style.backgroundColor = "red";
                    }
                }
            }
        });


        request.fail(function (jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });
	}
	
	
	//when seat is selected it validates whether that seat is booked or not
	function seatSelect(id)
	{	
		seatSelect.count = seatSelect.count || 0;   //To Check If Someone Is Changing the seat after selecting one
		var c = 0 ;
		var x;
		if(seatSelect.count > 0)
		{
			if(seatSelect.prevId == id)
			{
				if(document.getElementById(id).style.backgroundColor == "green")
				{
					document.getElementById(id).style.backgroundColor = "buttonface";
					c = 1;
					obj = {"seat":"null"};
					s = document.createElement("script");
					s.src = "php/selectSeat.php?x=" + JSON.stringify(obj);
					document.body.appendChild(s);
				}
			}
			
			else if(seatSelect.prevId != id)
			{
				//alert("here");
				//alert(seatSelect.prevId);
				document.getElementById(seatSelect.prevId).style.backgroundColor = "buttonface";
			}
		}
		
		for(x in seatDetails)
		{
			
			if(seatDetails[x].seat_no == id)
			{
				//alert(seatDetails[x].seat_no == id);
				alert("seat already booked");
				c = 1;
			}
		}
		
		
		if(document.getElementById(id).style.backgroundColor == "red")
		{
			alert("Not Allowed To Book This Seat");
			c = 1;
		}
		
		
		else if(c == 0)
		{
			//alert(id + " Seat Selected");
			var obj, s;
			var k,l;
			document.getElementById(id).style.backgroundColor = "green";
			obj = {"seat":id};
			seatSelect.prevId = id;
			seatSelect.count++;
			s = document.createElement("script");
			s.src = "php/selectSeat.php?x=" + JSON.stringify(obj);
			document.body.appendChild(s);
		}
	}