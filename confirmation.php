<?php
	session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Cinemax</title>
<link rel="icon" href="images/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="js/cufon-yui.js" type="text/javascript"></script>
<script src="js/cufon-replace.js" type="text/javascript"></script>
<script src="js/Gill_Sans_400.font.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="css/varunStyle.css"/>
<script type="text/javascript" src="js/myJs.js"></script>
<style type="text/css"><a href="confirmation.php">Cinema World</a>

#slogan{
	height: 400px !important;
}
a:hover{
	color:white !important;
}
.border-left{
	height:300px !important;
}
</style>

<!--[if lt IE 7]>
<script type="text/javascript" src="js/ie_png.js"></script>
<script type="text/javascript">ie_png.fix('.png, .link1 span, .link1');</script>
<link href="css/ie6.css" rel="stylesheet" type="text/css" />
<![endif]-->
</head>
<body id="page1">
<!-- START PAGE SOURCE -->
<div class="tail-top">
  <div class="tail-bottom">
    <div id="main">
      <div id="header">
        <div class="row-1">
          <div class="fleft"><a href="#">Cinema <span>World</span></a></div>
          	<div class="wel">
	            <span><a href="booktkt.php">Book Another One</a></span>
         		<span>Welcome <?php echo $_SESSION["Name"] ?> </span>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span><a href="./php/logout.php" style="color:#858585; text-decoration:none;">Logout</a></span>
            </div>
          
        </div>
       
      </div>
      <div id="content">
        <div id="slogan">
          <div class="inside seldate">
            <h1>Congratulation Your Ticket Has Been Booked.</h1><br /><br />
            <h1>Name:<?php echo $_SESSION["Name"]; ?></h1><br />
            <h1>SIC:<?php echo $_SESSION["SIC"]; ?></h1> <br />
            <h1>Seat No: <?php echo $_SESSION["current_selection"]; ?></h1><br />
            <h4 style="color:green;">*Please Bring Your ID Card N Screenshot of This Page at The Time Of Entry!!!</h4>

        </div>
      </div>
      <div id="footer">
        <div class="left">
          <div class="right">
            <div class="footerlink">
              <p class="lf">Copyright &copy; 2010 <a href="#">Cinemax</a> - All Rights Reserved</p>
              <p class="rf">Design by <a href="http://www.templatemonster.com/">Varun[IT]  Arunaya[IT]</a></p>
              <div style="clear:both;"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"> Cufon.now(); </script>
<!-- END PAGE SOURCE -->
</body>
</html>