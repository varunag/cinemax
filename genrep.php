<?php	
	session_start();
	if(isset($_SESSION["aplksyper"]) && $_SESSION["aplksyper"] == 1)
	{
		/*echo"<script>window.alert('Welcome Admin')</script>";*/
	}
	else{
		echo"<script>window.alert('You Are Not Authorized To Access This Web Page!!!!Please LogIn To Continue')</script>";
		echo"<script>window.open('admin.php','_self')</script>"; 
	}
	
?>

<html>
	<head>
    	<title>
        	Report Generation
        </title>
<link rel="icon" href="images/favicon.ico" />
		<script type="text/javascript">
        function testdate()
        {
            var d = new Date(document.getElementById("selDate").value);
            var obj, s;
            mo = d.getMonth()+1;
            if(mo>=1&&mo<=9)
            {
                fDate = d.getFullYear() + "-" + 0+mo + "-" + d.getDate();
            }
            else
            {
                fDate = d.getFullYear() + "-" + mo + "-" + d.getDate();
            }
            //alert(fDate);
            obj = {"date":fDate};
            s = document.createElement("script");
            s.src = "php/getList.php?x=" + JSON.stringify(obj);
            document.body.appendChild(s);
        }
        
        
        //Shows Whether There Is Any Movie On that day and if there is a movie how many shows are present
        function myFunc(myObj) {
            var x;
            if(myObj.length != 0)
            {
                document.getElementById("mLabel").hidden = false;
                document.getElementById("mName").hidden = false;
                document.getElementById("showList").hidden = false;
                document.getElementById("mName").innerHTML = myObj[0].name;
                var txt = "";
                for(i = 1; i <= myObj[0].no_of_shows; i++ )
                {
                    txt = txt + '<input type="radio" name="show" id="show' + i + '" onclick="showInfo(this.id)" />Show ' + i + '<br />';	
                }
                
                document.getElementById("showList").innerHTML = txt;
            }
            else{
                alert("Not On This Day");
                document.getElementById("seatLayout").hidden = true;
                document.getElementById("mName").hidden = true;
    ;			document.getElementById("mLabel").hidden = true;
                document.getElementById("showList").hidden = true;
            }
        }
        function showInfo(id)
        {
            var s = document.getElementById("showno");
            s.value = id;
        }
		</script>
        
        <style>
			body{
				background:url(images/genrep3.jpg);
				background-repeat:no-repeat;
				background-size:cover;
			}
			.button {
				background-color: #4CAF50; /* Green */
				border: none;
				color: white;
				padding: 16px 32px;
				text-align: center;
				text-decoration: none;
				display: inline-block;
				font-size: 16px;
				margin: 4px 2px;
				-webkit-transition-duration: 0.4s; /* Safari */
				transition-duration: 0.4s;
				cursor: pointer;
			}
			
			.button1 {
				background-color: #4CAF50; 
				color: white; 
				border: 2px solid #4CAF50;
			}
			
			.button1:hover {
				background-color: white;
				color: black;
			}
			
			.button2 {
				background-color: #008CBA; 
				color: white; 
				border: 2px solid #008CBA;
			}
			
			.button2:hover {
				background-color: white;
				color: black;
			}
			
			.button3 {
				background-color: #f44336; 
				color: white; 
				border: 2px solid #f44336;
			}
			
			.button3:hover {
				background-color: #f44336;
				color: black;
			}
			
			#navbar{
				text-align:center;
			}
			#formdata{
				font-family:"Courier New", Courier, monospace;
				padding-left:40%;
				color:#000033;
				font-size:20px;
			}
			.box{
				border-color:red;
				border-radius:4px;
				padding:12px 20px;
			}
			
			
			
			input[type=submit]{
				background-color: #4CAF50; /* Green */
				border: none;
				color: white;
				width:120px;
				padding: 10px 20px;
				text-align: center;
				text-decoration: none;
				display: inline-block;
				font-size: 16px;
				margin: 4px 2px;
				-webkit-transition-duration: 0.4s; /* Safari */
				transition-duration: 0.4s;
				cursor: pointer;
				background-color: #555555; 
				color: white; 
				border: 2px solid #555555;
			}
			input[type=submit]:hover {
				background-color: white;
				color: black;
			}
			#space{
				padding:75px;
			}
			a{
				float:right;
				color:#666;
			}
		}
		</style>

</head>
<body> 
		<div id="navbar">
          <button class="button button1" onClick="window.open('entermovie.php','_self');">Movie Entry</button>
          <button class="button button2" onClick="window.open('genrep.php','_self');">Report Generation</button>
          <button class="button button3" onClick="window.open('memberentry.php','_self');">Enter New Member</button>
          <a href="logoutadmin.php">logout</a>
        </div>  
        <br /><br /><br />
        
        <div id="formdata">
        <form action="report.php" method="post">
            Select Date:<input type="date" class="box" id="selDate" name="movdate" onChange="testdate(this,true)" /><br /><br>
            <span id="mLabel" hidden="true">Movie Name:</span><span id="mName"></span><br />
            <p id="showList" ></p>
            <input type="text" hidden="true" name="showno" id="showno"/><br>
            <span id="space"></span><input type="submit" name="submit" value="Generate" />
        </form>
    </body>
</html>