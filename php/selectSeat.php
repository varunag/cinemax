<?php
	session_start();
	header("Content-Type: application/json; charset=UTF-8");
	$obj = json_decode($_GET["x"], false);
	$selSeat = $obj->seat;
	if($selSeat == "null")
	{
		$_SESSION["current_selection"] = "null";
	}
	else
	{
		$_SESSION["current_selection"] = $selSeat;
	}
?>